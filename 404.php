<?php
/**
 * The template for displaying the 404 template in the Twenty Twenty theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main>
		<?php 
			$args = array(
				'numberposts' => 10
			);
			
			$latest_books = get_posts( $args );
		?>
</main><!-- #site-content -->

<?php
get_footer();