<?php get_header(); ?>

<main class="container">

<div id="site">
  
  <section class="articles">
		<?php
			$args = array( 
				'post_type' => 'post'
			); 
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<div class="article" id="Hola<?php the_ID(); ?>">
					<div class="inner">
						<img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt=" <?php the_title(); ?>">
						<p class="article-content"><?php the_title(); ?></p>
					</div>
				</div>
		
			
		<?php endwhile; wp_reset_query(); ?>

  </section>
  
	 <?php
			$args = array( 
				'post_type' => 'post'
			); 
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<main class="content" id="ArctHola<?php the_ID(); ?>">
				<h1 class="heading"><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</main>
		<?php endwhile; wp_reset_query(); ?> 
  
</div>
</main><!-- #site-content -->

<script src="<?php echo get_bloginfo('template_directory'); ?>/js/scriptHome.js"></script>
<?php get_footer(); ?>
